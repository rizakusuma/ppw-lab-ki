from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request, 'Home.html')

def about(request):
    return render(request, 'About.html')

def contact(request):
    return render(request, 'Contact.html')

def registration(request):
    return render(request, 'Registration.html')


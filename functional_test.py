from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

def test_title(self):
    selenium = self.selenium
    selenium.get("https://scele.cs.ui.ac.id/")
    time.sleep(2)
    searchbox = selenium.find_element_by_id("coursesearchbox")
    searchbox.send_keys("PPW")
    time.sleep(2)
    searchbox.send_keys(Keys.ENTER)
    time.sleep(2)
    self.assertIn("Search results: 6", selenium.find_element_by_css_selector("#region-main div h2").text)
    self.assertIn("[KI] Perancangan & Pemrograman Web", selenium.find_element_by_css_selector(".courses.course-search-result").text)

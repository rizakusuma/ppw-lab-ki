# Where do we start from here?


# This root directory consists of:
* **requirements.txt** : Python modules list, used when we call `pip install -r requirements.txt`
* **manage.py** : The Main Program that will run your code in sub-directory lab_1, lab_2, etc
* **.gitlab-ci.yml** : Required script that will only run on __GitLab.com__. This script will do the auto-testing & auto-deployment in GitLab server.
* **Procfile** : Required script that will only run on __Herokuapp.com__
* **README.md** : This Documentation (on markdown format)
